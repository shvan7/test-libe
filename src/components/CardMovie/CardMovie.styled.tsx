import styled from 'styled-components'
import Card from '@mui/material/Card'

export const SCCard = styled(Card)`
  position: relative;
  cursor: pointer;
  background-color: white;

  &:hover {
    > img {
      opacity: 0.2;
    }

    > h4 {
      transform: translateY(10%);
    }

    > div {
      transform: translateY(1%);
    }
  }
`

export const TitleMovie = styled.h4`
  transform: translateY(-200%);
  position: absolute;
  color: black;
  overflow-wrap: break-word;
  padding: 5px 10px;
  width: 85%;
  top: 0;
  margin: 3px 5px;
  transition: transform 0.2s ease-in;
`

export const ContentLightInfos = styled.div`
  position: absolute;
  bottom: 0;
  transition: transform 0.2s ease-in;
  transform: translateY(200%);
  display: flex;
  justify-content: center;
  flex-direction: column;
  width: 100%;

  > p {
    color: black;
    text-align: center;
  }

  button {
    color: black;
  }
`
