import React from 'react'
import CardMedia from '@mui/material/CardMedia'
import { createUrlImageMovie } from '../../constant/api.config'
import { SCCard, TitleMovie, ContentLightInfos } from './CardMovie.styled'
import { Button } from '@mui/material'
import dayjs from 'dayjs'

const CardMovie = ({ data }) => {
  const { title, backdrop_path, poster_path, release_date } = data

  const dateFormated = dayjs(release_date).format('DD MMMM YYYY')

  console.log(data)
  return (
    <SCCard>
      <TitleMovie>{title}</TitleMovie>
      <CardMedia
        component="img"
        height={280}
        image={createUrlImageMovie(500, poster_path)}
        alt="green iguana"
      />
      <ContentLightInfos>
        <p>{dateFormated}</p>
        <Button size="small">More</Button>
      </ContentLightInfos>
    </SCCard>
  )
}

export default CardMovie
