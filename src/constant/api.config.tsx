import axios from 'axios'

const API_MOVIE = 'https://api.themoviedb.org/3'

export const createUrlImageMovie = (width, path) =>
  `http://image.tmdb.org/t/p/w${width}/${path}`

const apiClient = axios.create({
  baseURL: API_MOVIE,
  headers: {
    'Content-type': 'application/json',
  },
})

apiClient.interceptors.request.use((config) => {
  config.params = {
    api_key: '24d31be03ba7e6417a6d18a8a303f4e4',
    ...config.params,
  }
  return config
})

export default apiClient
