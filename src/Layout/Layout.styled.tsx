import styled from 'styled-components'

export const ContentSortMenu = styled.div`
  button {
    color: #0e8ed8;
  }

  ul {
    background-color: red;
  }
`

export const MainLayout = styled.div`
  height: 100vh;
  width: 100vw;

  background-image: linear-gradient(
    to bottom,
    rgba(0, 0, 0, 0.9) 0%,
    var(--red-3) 100%
  );
`

export const Navbar = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 6vh;
  position: fixed;
  top: 0;
  z-index: 999;
  width: 100%;
  background-image: linear-gradient(
    to bottom,
    rgba(0, 0, 0, 0.9) 0%,
    rgba(0, 0, 0, 0.5) 100%
  );

  > h1 {
    color: var(--silver-1);
    font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande',
      'Lucida Sans', Arial, sans-serif;

    span {
      color: #0e8ed8;
    }
  }
`

export const Content = styled.div`
  display: flex;
  flex-wrap: wrap;
  transition: all 2s ease;
  max-height: 94vh;
  overflow-y: auto;
  padding: 10px;
  padding-top: 6vh;

  > div {
    flex: 1 0 180px;
    border-radius: 4px;
    margin: 2px;
    box-sizing: border-box;

    @media screen and (max-width: 480px) {
      min-width: 80vw;
    }
  }
`
