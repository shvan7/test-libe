import React from 'react'
import { MainLayout, Navbar, Content, ContentSortMenu } from './Layout.styled'
import { CardMovie } from '../components'
import apiClient from '../constant/api.config'
import { useQuery } from 'react-query'
import Button from '@mui/material/Button'
import Menu from '@mui/material/Menu'
import MenuItem from '@mui/material/MenuItem'
import dayjs from 'dayjs'

const SortMenu = () => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null)
  const open = Boolean(anchorEl)

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget)
  }

  const handleClose = () => {
    setAnchorEl(null)
  }

  return (
    <ContentSortMenu>
      <Button
        id="basic-button"
        aria-controls={open ? 'basic-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
      >
        MENU
      </Button>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        <MenuItem onClick={handleClose}>Sort by date</MenuItem>
      </Menu>
    </ContentSortMenu>
  )
}

const Layout = () => {
  const lastMovies = useQuery('query-lastest-movies', async () => {
    return await apiClient.get('/discover/movie', {
      params: {
        year: dayjs().format('YYYY'),
      },
    })
  })

  const movies = lastMovies?.data?.data?.results ?? []

  const renderCard = () => {
    return movies.map((e, index) => {
      return <CardMovie key={index + 'card-movie'} data={e} />
    })
  }

  return (
    <MainLayout>
      <Navbar>
        <SortMenu />
        <h1>
          Mov <span>E</span>
        </h1>
        <div></div>
      </Navbar>
      <Content>{renderCard()}</Content>
    </MainLayout>
  )
}

export default Layout
